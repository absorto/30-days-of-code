class Difference:
	def __init__(self, a):
		self.__elements = a

	def computeDifference(self):
		sort_El = sorted(self.__elements)
		self.maximumDifference = abs(sort_El[0] - sort_El[-1])


# End of Difference class

_ = input()
a = [int(e) for e in input().split(' ')]

d = Difference(a)
d.computeDifference()

print(d.maximumDifference)
