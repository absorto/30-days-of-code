#!/bin/python3

# Complete the plusMinus function below.
def plusMinus(n, arr):
	"""
	Given an array of integers, calculate the fractions of its elements that
	are positive, negative, and are zeros. Print the decimal value of each 
	fraction on a new line.
	""" 

	size_arr = int(n)
	pos, neg, zero= 0, 0, 0

	# check how many positive, negative and zero int on list
	for i in arr:
		if i == 0:
			zero += 1
		elif i > 0:
			pos += 1
		else:
			neg += 1
	pos = pos/size_arr
	neg = neg/size_arr
	zero = zero/size_arr
	print(pos, neg, zero)
	return pos, neg, zero

if __name__ == '__main__':
	n = int(input())
	#arr = list(map(int, input().rstrip().split()))
	arr = [-4, 3, -9, 0, 4, 1]
	plusMinus(n, arr)
