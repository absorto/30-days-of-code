'''
Task
Given a string, S , of length that is indexed from to , 
print its even-indexed and odd-indexed characters as space-separated strings on 
a single line (see the Sample below for more detail).

Note: is considered to be an even index.

Input Format

The first line contains an integer, (the number of test cases).
Each line of the subsequent lines contain a String, . 
'''

# Enter your code here. Read input from STDIN. Print output to STDOUT

n = int(input())
count = 1
print(n)
print(count)

while count <= n:
	print('dentro do loop')
	word = input()
	for letter in word:
		print(letter)
		sEven = ''
		sOdd = ''
		for i in range(0,len(word),2):
			sEven = sEven + word[i]
		for i in range(1,len(word),2):
			sOdd = sOdd + word[i]
	count += 1
	print(sEven, sOdd)


'''
stdin = int(input())
if 1 <=stdin and stdin <= 10:
    for i in range(stdin):
        word = str(input())
        if 2 <= len(word) and len(word) <= 10000:
            print(word[::2], word[1::2])

'''
