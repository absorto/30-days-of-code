'''
Given five positive integers, find the minimum and maximum values that can be 
calculated by summing exactly four of the five integers. Then print the 
respective minimum and maximum values as a single line of two space-separated 
long integers.

For example, arr = [1,3,5,7,9].
Our minimum sum is 1+3+5+7 = 16 and our maximum sum is 3 + 5 + 7 + 9 = 
24. We would print

16 24

Function Description

Complete the miniMaxSum function in the editor below. It should print two 
space-separated integers on one line: the minimum sum and the maximum sum 
of elements.

miniMaxSum has the following parameter(s):

arr: an array of integers

Input Format

A single line of five space-separated integers.

Output Format

Print two space-separated long integers denoting the respective minimum and 
maximum values that can be calculated by summing exactly four of the five 
integers. (The output can be greater than a 32 bit integer.)

Example Input

1 2 3 4 5 
'''
arr =  [1,2,3,4,5]

#def minimum(arr):
	#b = arr
	#print('minimum',b)
	#minimum = 0
	#b.sort()
	#print(b.pop(-1))
	#print('minimum',b)
	#for item in b:
		#minimum += item
	#return minimum

#def maximum(arr):
	#a = arr
	#print('maximum',a)
	#maximum = 0
	#a.sort()
	#print(a.pop(0))
	#print('maximum',a)
	#for item in a:
		#maximum += item
	#return maximum


#print(minimum(arr))
#print(maximum(arr))

mini = 0
maxi = 0

i = [1,2,3,4,5]
i.sort()
i.pop(-1)
for item in i:
	mini += item
print(mini)

a = [1,2,3,4,5]
a.sort()
a.pop(0)
for item in a:
	maxi += item
print(maxi)

