#!/bin/python3

import os
import sys

def timeConversion(s):
	"""
	 00 <= HH <= 23
	""" 
	print('===========')
	print('original', s)
	# for AM nothing needs to change
	if "AM" in s:
		s = s[:-2]
	# if PM then we need to convert it
	else:
		if int(s[:2]) == 12:
			new_hh = '00'
		else:
			new_hh = (int(s.split(':')[0]) + 12)
		s = s.replace(s[:2],str(new_hh),1)[:-2]
	print(s)
	return s

s_list = ['01:05:45PM','02:05:45PM','03:05:45PM','04:04:45PM','05:05:45PM','06:05:45PM',
'07:05:45PM','08:05:45PM','09:05:45PM','10:05:45PM','11:05:45PM','12:05:59PM',
'12:05:45AM','01:05:45AM','02:05:45AM','03:05:45AM','04:05:00AM','05:05:45AM',
'06:05:45AM','07:05:45AM','08:05:45AM','09:05:45AM','10:05:45AM','11:05:45AM',
]


for item in s_list:
	timeConversion(item)


