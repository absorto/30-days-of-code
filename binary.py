'''
Given a base 10 integer, n, convert it to binary (base-2). Then find and print the 
base-10 integer denoting the maximum number of consecutive 1's in n's binary 
representation.
'''


n = int(input())

buff = [] 
b = bin(n)[2:].split('0')
for item in b:
	buff.append(len(item))
print(max(buff))

